from playground.twisted.error.ErrorHandlers import TwistedShutdownErrorHandler
from twisted.internet import reactor
from ..firmware.controller import Controller
from ..firmware.playground_interface import BotClientEndpoint, BotChaperoneConnection
from twisted.internet import reactor
from playground.network.common.PlaygroundAddress import PlaygroundAddress
from ..firmware.ReprogrammableData import ReprogrammableData, RawDataLoader, PythonModuleLoader
from ..firmware.gameloop_interface import PlaygroundOutboundSocket
from ..firmware.gameloop_interface import PlaygroundSocketProtocolFactory,GameLoopCtx

def run(details):
    controller = Controller()
    controller.connectToChaperone(details)
    TwistedShutdownErrorHandler.HandleRootFatalErrors()
    reactor.run()

chaperone_addr='192.168.11.233'
chaperone_port=9090
#run((chaperone_addr, chaperone_port))



class AttackController(Controller):
    botaddr='20171.4399.4399.4399'
    botport='666'
    serveraddr='20171.11.111.1111'
    serverport='10001'
    def __init__(self):
        self.__botData = ReprogrammableData()

        # REPORGRAMMABLE DATA #1: ADDRESS #
        self.__botData.address = self.__botData.createModule("ADDRESS")
        self.__botData.address.manifest.append("address.txt")
        self.__botData.address.loader = RawDataLoader(self.__botData.address)

        # REPROGRAMMABLE DATA #3: CERTFACTORY #
        self.__botData.certFactory = self.__botData.createModule("CERT_FACTORY")
        self.__botData.certFactory.manifest.append("CertFactory.py")
        self.__botData.certFactory.loader = PythonModuleLoader(self.__botData.certFactory, "CertFactory")
        self.__botData.certFactory.loader.setPostLoadOperation(lambda *args: self.__reloadCertificates())

        # REPROGRAMMABLE DATA #4: PROTOCOLSTACK #
        self.__botData.protocolStack = self.__botData.createModule("PROTOCOL_STACK")
        self.__botData.protocolStack.manifest.append("ProtocolStack/__init__.py")
        self.__botData.protocolStack.loader = PythonModuleLoader(self.__botData.protocolStack, "ProtocolStack")

    def outConnect(self):
        botData=self.__botData
        if self.__botData.protocolStack():
            PlaygroundOutboundSocket.PROTOCOL_STACK = lambda self, *args, **kargs: botData.protocolStack().ConnectFactory.Stack(*args, **kargs)

        BotChaperoneConnection.ConnectToChaperone(reactor, (chaperone_addr, chaperone_port), PlaygroundAddress.FromString(self.botaddr))
        endpoint = BotClientEndpoint(self.serveraddr, self.serverport)
        if PlaygroundOutboundSocket.PROTOCOL_STACK:

            # python weirdly won't let a class variable point to a regular function.
            # have to manually pass self as the first argument.
            print "Trying to connect using protocol stack"
            stack = PlaygroundOutboundSocket.PROTOCOL_STACK(self, PlaygroundSocketProtocolFactory())
        else:
            print "trying to connect raw"
            stack = PlaygroundSocketProtocolFactory()
        print "connect stack", stack
        d = endpoint.connect(stack)


class AttackController2(Controller):
    def __init__(self):
        pass
    def cAndCCon(self, addr):
        ctx = GameLoopCtx()
        cAndC = ctx.socket()
        cAndC.connect(addr, 10001)

cAndCServerAddr='20171.11.111.1111'
ac2=AttackController2('')
ac2.cAndCCon(cAndCServerAddr)
#ac=AttackController()
#ac.outConnect()
#reactor.run()
