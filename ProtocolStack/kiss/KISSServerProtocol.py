from KISSBaseProtocol import KissBaseProtocol
from KISSTransport import KissTransport
from KISSConfig import DATATRANSMISSION

class KissServerProtocol(KissBaseProtocol):
    def __init__(self):
        super(KissServerProtocol, self).__init__('server')

    def hsRecvPacket(self, kissHandshake):
        super(KissServerProtocol, self).hsRecvPacket(kissHandshake)
        self.hsSendPacket()
        self.status = DATATRANSMISSION
        self.makeHigherConnection(self.higherTransport)
