from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import UINT4, OPTIONAL, STRING, DEFAULT_VALUE, LIST, BOOL1

class RipMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "RIP.RIPMessageID"
    MESSAGE_VERSION = "1.0"

    BODY = [
        ("sequence_number", UINT4),

        ("acknowledgement_number", UINT4, OPTIONAL),

        ("signature", STRING, DEFAULT_VALUE("")),

        ("certificate", LIST(STRING), OPTIONAL),

        ("sessionID", STRING),

        ("acknowledgement_flag", BOOL1, DEFAULT_VALUE(False)),

        ("close_flag", BOOL1, DEFAULT_VALUE(False)),

        ("sequence_number_notification_flag", BOOL1,
         DEFAULT_VALUE(False)),

        ("reset_flag", BOOL1, DEFAULT_VALUE(False)),

        ("data", STRING, DEFAULT_VALUE("")),

        ("OPTIONS", LIST(STRING), OPTIONAL)
    ]

