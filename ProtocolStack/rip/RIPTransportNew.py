from playground.network.common.Protocol import StackingTransport
from RIPConfig import HANDSHAKE_STEP0, HANDSHAKE_STEP1, DATA_TRANSPORT, DEBUG, STRICT
from RIPConfig import MAX_SEQUENCE_NUMBER, MAX_NONCE_NUMBER, MAX_SEGMENT_SIZE
from RIPConfig import logger
from RIPConfig import SENDER_WINDOW_SIZE, RECEIVER_WINDOW_SIZE, TIMEOUT, TIMEOUT_HANDSHAKE, SENDER_BUFFER_TIMER_NUMBER
from RIPMessage import RipMessage
from playground.network.common.Timer import OneshotTimer, callLater
from RIPConfig import randomErrorHappen
from RIPConfig import ERROR_TEST
from playground.crypto.PkiPlaygroundAddressPair import PkiPlaygroundAddressPair
from RIPCrypto import RipCrypto

class RipTransport(StackingTransport):
    protocol = None
    timer = None
    timerForHs = None
    dataBuffer = ""
    sndBuffer = None
    sndBufferForHs = None  # send buffer for handshake
    def __init__(self, lowerTransport, protocol):
        StackingTransport.__init__(self, lowerTransport)
        self.protocol = protocol
        self.timer = OneshotTimer(self.resend, False)
        self.timerForHs = OneshotTimer(self.resend, True)

    def getHost(self):
        hostpair = super(RipTransport, self).getHost()
        # certs[0]=A.B.C.D certs[1]=A.B.C
        certs = [RipCrypto.getX509ObjectFromBytes(self.protocol.certs[i]) for i in [0,1]]
        rsa = RipCrypto.getRsaObjectFromBytes(self.protocol.mySkBytes)
        return PkiPlaygroundAddressPair.ConvertHostPair(hostpair, certs, rsa)

    def getPeer(self):
        peerpair = super(RipTransport, self).getPeer()
        certs = [RipCrypto.getX509ObjectFromBytes(self.protocol.peerCerts[i]) for i in [0,1]]
        return PkiPlaygroundAddressPair.ConvertPeerPair(peerpair, certs)

    def resend(self, forHs=False):
        logger.log('info', 'resend, for handshake: ' + str(forHs))
        if forHs:
            self.writeRipMessage(self.sndBufferForHs)
        else:
            self.writeRipMessage(self.sndBuffer)
        self.timerStart(forHs)

    def timerStart(self, forHs=False):
        self.protocol.allAckRcv = False
        if forHs:
            self.timerForHs.run(TIMEOUT_HANDSHAKE)
        else:
            self.timer.run(TIMEOUT)

    def timerStop(self, forHs=False):
        if forHs:
            self.timerForHs.cancel()
        else:
            self.timer.cancel()
        self.protocol.allAckRcv = True
        self.dataBuffer and callLater(0, self.write, '')


    def getSegmentData(self):
        if len(self.dataBuffer) >= MAX_SEGMENT_SIZE:
            segData = self.dataBuffer[:MAX_SEGMENT_SIZE]
            bytesUsed = MAX_SEGMENT_SIZE
        else:
            segData = self.dataBuffer
            bytesUsed = len(self.dataBuffer)
        return segData, bytesUsed

    def write(self, data):
        self.dataBuffer +=data

        if self.timer.started():
            return

        segData, bytesUsed = self.getSegmentData()
        self.dataBuffer = self.dataBuffer[bytesUsed:]

        ripMessage = RipMessage()
        ripMessage.sequence_number = self.protocol.seqNum
        ripMessage.acknowledgement_number = 0
        ripMessage.certificate = []
        ripMessage.sessionID = self.protocol.sessionId
        ripMessage.acknowledgement_flag = False
        ripMessage.close_flag = False
        ripMessage.sequence_number_notification_flag = False
        ripMessage.reset_flag = False
        ripMessage.data = segData
        ripMessage.OPTIONS = []
        ripMessage.signature = ""

        signature_data = ripMessage.__serialize__()
        signature = self.protocol.crypto.sign(signature_data)
        ripMessage.signature = signature

        self.writeRipMessage(ripMessage)

        self.protocol.seqNum += len(ripMessage.data)
        self.sndBuffer = ripMessage
        self.timerStart(False)
        # ...
        pass

    def writeRipMessage(self, ripMessage):
        if ERROR_TEST:
            if not randomErrorHappen():
                self.lowerTransport().write(ripMessage.__serialize__())
            logger.log('snd', ripMessage)
            return
        self.lowerTransport().write(ripMessage.__serialize__())
        logger.log('snd', ripMessage)

    def loseConnection(self):
        if self.dataBuffer or not self.protocol.allAckRcv:
            callLater(1, self.loseConnection)
        else:
            self.protocol.closeAndTimer()
            # self.higherTransport.

    def loseConnectionReal(self):
        logger.log('info', 'loseConnection')
        super(RipTransport, self).loseConnection()
