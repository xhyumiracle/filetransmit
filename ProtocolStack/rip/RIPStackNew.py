'''
Created on Oct 18, 2016

@author: xhyu
'''
from twisted.internet.protocol import Factory
from playground.network.common.Protocol import  StackingFactoryMixin
from RIPClientProtocolNew import RipClientProtocol
from RIPServerProtocolNew import RipServerProtocol

class RipClientFactory(StackingFactoryMixin, Factory):
    protocol = RipClientProtocol


class RipServerFactory(StackingFactoryMixin, Factory):
    protocol = RipServerProtocol

# TODO: no sign in handshake

# ConnectFactory = RipClientFactory
# ListenFactory = RipServerFactory
def ConnectFactory():
    kissFactory = KissClientFactory()
    ripFactory = RipClientFactory()
    ripFactory.setHigherFactory(kissFactory)
    return ripFactory

def ListenFactory():
    kissFactory = KissServerFactory()
    ripFactory = RipServerFactory()
    ripFactory.setHigherFactory(kissFactory)
    return ripFactory

# ConnectFactory = getConnectFactory()
# ListenFactory = getListenFactory()
