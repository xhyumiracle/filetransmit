from bot.common.network import ReprogrammingRequest
import md5
import collections

class PwdBreaker:
    __originalChecksum = ""
    __passwdRange = []

    def __deserialize(self, s):
        req, bytesUsed = ReprogrammingRequest.Deserialize(s) # where 's' is the string above
        return req, bytesUsed

    def __extChecksumFromRequestPacket(self, req):
        return req.Checksum


    def __calChecksumOfPacket(self, packet):
        return md5.new(packet.__serialize__()).hexdigest()

    def __isMatched(self, c):
        if self.__originalChecksum == c:
            return True
        else:
            return False

    def __rec(self, totalbits, bits, slist):
        if bits == 0:
            yield "".join(slist)
            return
        for i in range(10):
            slist[totalbits-bits] = chr(ord('0')+i)
            yield self.__rec(totalbits, bits - 1, slist)

    def __generatePwdRange(self, bits, s):
        iters = self.__rec(bits, bits, list(s))
        for i in self.flattern(iters):
            yield i

    def flattern(self, it, what_type=str):
        for o in it:
            if not isinstance(o, str) and isinstance(o, collections.Iterator):
                for i in self.flattern(o):
                    yield i
            else:
                yield o

    def bruteForce(self, s):
        realpwd = ''
        req, _ = self.__deserialize(s)
        self.__originalChecksum = self.__extChecksumFromRequestPacket(req)
        # pwdit = self.__generatePwdRange(6, 6, list("000000"))
        for testpwd in self.__generatePwdRange(6, "000000"):
            req.Checksum = testpwd
            testChecksum = self.__calChecksumOfPacket(req)
            # print "testing: ", testpwd, testChecksum, self.__originalChecksum
            if list(testpwd)[2:] == list("0000"):
                print "testing:", testpwd
            if self.__isMatched(testChecksum):
                realpwd = testpwd
                break
        if realpwd == '':
            print "failed"
            return False, "failed to find real pwd in current range"
        else:
            print "Password is [%s]" % realpwd
            return True, realpwd


    def test(self):
        for i in self.__generatePwdRange(3, "000"):
            print 'haha', i

    def bruteForce1(self, s, echecksum):
        realpwd = ''
        req, _ = self.__deserialize(s)
        #self.__originalChecksum = self.__extChecksumFromRequestPacket(req)
        self.__originalChecksum = echecksum#self.__extChecksumFromRequestPacket(req)
        # pwdit = self.__generatePwdRange(6, 6, list("000000"))
        for testpwd in self.__generatePwdRange(6, "000000"):
            req.Checksum = testpwd
            testChecksum = self.__calChecksumOfPacket(req)
            # print "testing: ", testpwd, testChecksum, self.__originalChecksum
            if list(testpwd)[2:] == list("0000"):
                print "testing:", testpwd
            if self.__isMatched(testChecksum):
                realpwd = testpwd
                break
        if realpwd == '':
            print "failed"
            return False, "failed to find real pwd in current range"
        else:
            print "Password is [%s]" % realpwd
            return True, realpwd


    def test(self):
        for i in self.__generatePwdRange(3, "000"):
            print 'haha', i

breaker = PwdBreaker()

# breaker.test()
if __name__=="__main__":
    breaker = PwdBreaker()
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("checksum", nargs=1)

    opts = parser.parse_args()
    checksumstr = opts.checksum
    breaker.bruteForce1(checksumstr)

