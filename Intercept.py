from playground.twisted import endpoints
from twisted.internet.protocol import Protocol, Factory
from twisted.internet import reactor
from PwdBreak import breaker

class DumpProtocol(Protocol):
    def dataReceived(self, data):
        print "TAP received %d bytes from %s to %s" % (len(data), self.transport.getPeer(), self.transport.getHost())
        print data
        #checksum=raw_input("input checksum:")
        #checksum=raw_input("input checksum:")
        #print "checksum: ", checksum
        breaker.bruteForce1(data, checksum)

class DumpFactory(Factory):
    protocol = DumpProtocol

def tap(address, port, gateTcpPort=9091):
    settings = endpoints.PlaygroundNetworkSettings()
    settings.changeGate(gateTcpPort=gateTcpPort)
    settings.requestSpecificAddress(address)

    tap = endpoints.GateServerEndpoint(reactor, port, settings)

    protocolFactory = DumpFactory()

    tap.listen(protocolFactory)

if __name__=="__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("target", nargs=2)
    parser.add_argument("--gate-port", type=int, default=9091)

    opts = parser.parse_args()
    tapAddress, tapPort = opts.target
    tapPort = int(tapPort)

    print "Starting simple playground 'wiretap'"
    tap(tapAddress, tapPort, gateTcpPort=opts.gate_port)
    reactor.run()

